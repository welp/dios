#!/bin/sh

EMAIL="$1"

if [ -z ${EMAIL+x} ]; then
  echo "--- ERROR: no email address specified"
  exit 1
fi

PG_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/${POSTGRES_DB}"

psql "${PG_URL}" \
     -c "UPDATE users SET streamer = 'true' WHERE email = '${EMAIL}'"
