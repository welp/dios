#!/bin/sh

PG_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/${POSTGRES_DB}"

diesel migration run --database-url="${PG_URL}"
