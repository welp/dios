use std::sync::RwLock;

pub struct StreamInfo {
    pub title: RwLock<Option<String>>,
    pub game: RwLock<Option<String>>
}

impl Default for StreamInfo {
    fn default() -> StreamInfo {
        StreamInfo {
            title: RwLock::new(Option::None),
            game: RwLock::new(Option::None)
        }
    }
}
