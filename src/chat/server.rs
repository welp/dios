use rand::{self, rngs::ThreadRng, Rng};
use std::collections::{HashMap, VecDeque};
use std::time::{Duration, Instant};

use actix_identity::Identity;
use actix_http::*;
use actix::prelude::*;
use actix_web::*;
use actix_web_actors::ws;

use crate::ConnectionPool;
use crate::chat::sessions;
use crate::messages;
use crate::stream_info::StreamInfo;
use crate::utils::user::*;
use crate::utils::mail::*;

/// How often heartbeat pings are sent
pub const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
/// How long before lack of client response causes a timeout
pub const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);
/// How many past chat messages to store in the log
pub const MAX_LOG_SIZE: usize = 100;

#[derive(Message)]
#[rtype(result = "()")]
pub struct Message(pub String);

#[derive(Message)]
#[rtype(result = "()")]
pub struct Session {
    recipient: Recipient<Message>,
    user: String
}

#[derive(Message)]
#[rtype(usize)]
pub struct Connect {
    pub address: Recipient<Message>,
    pub user: String
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct Disconnect {
    pub id: usize,
}

pub struct ChatServer {
    sessions: HashMap<usize, Session>,
    rng: ThreadRng,
    log: VecDeque<messages::Envelope<messages::ChatMessage>>,
    stream_info: web::Data<StreamInfo>,
    pool: ConnectionPool

}


impl ChatServer {
    pub fn new(server_state: web::Data<StreamInfo>, pool: ConnectionPool) -> ChatServer {
        ChatServer {
            sessions: HashMap::new(),
            rng: rand::thread_rng(),
            log: VecDeque::with_capacity(MAX_LOG_SIZE),
            stream_info: server_state,
            pool: pool
        }
    }

    fn send_message_to_all(&self, message: &str) {
        ////Send a raw message to all clients.
        for (_id, session) in &self.sessions {
            let _ = session.recipient.do_send(Message(message.to_owned()));
        }
    }

    fn send_message_to_one(&self, message: &str, target_id: usize ) {
        if let Some(session) = self.sessions.get(&target_id) {
            let _ = session.recipient.do_send(Message(message.to_owned()));
        }
    }

    fn send_system_message(&self, message: &str, user: &str) {
        ////Create a notification message and send it to a client.
        ////Higher level than send_message_to_one, which sends a raw message.

        let message =  messages::Envelope::new(messages::ServerMessage{
            message: message.to_owned(),
            recipient: user.to_owned()}, None);

        //hacky
        for (id, session) in &self.sessions {
            if session.user == user {
                self.send_message_to_one(&serde_json::to_string(&message).unwrap(), *id)
            }
        }
    }

    fn _find_message_log(&self, message_id: u32) -> Option<&messages::Envelope<messages::ChatMessage>> {

        let mut found_message: Option<usize> = Option::None;
        for (i, message) in self.log.iter().enumerate() {

            if message.message_id == message_id {
                found_message = Option::Some(i);
                break;
            }
        }

        if let Option::Some(i) = found_message {
            return Some(self.log.get(i).unwrap());
        }
        else {
            return None;
        }
    }

    fn find_user_messages(&self, username: &str) -> Vec<u32> {
        let mut found_messages = Vec::new();

        for message in self.log.iter() {
            if message.user.as_ref().unwrap() == username {
                found_messages.push(message.message_id);
            }
        }
        return found_messages;
    }

    fn delete_message_log(&mut self, message_id: u32) {
        let mut found_message: Option<usize> = Option::None;
        for (i, message) in self.log.iter().enumerate() {

            if message.message_id == message_id {
                found_message = Option::Some(i);
                break;
            }
        }

        if let Option::Some(id) = found_message {
            self.log.remove(id);
        }
        else {
            println!("Message not found!");
        }
    }

    fn delete_user_messages(&mut self, username: &str) {
        let messages =  self.find_user_messages(username);

        for message_id in messages {
            self.delete_message_log(message_id);
        }
    }

    fn remove_client_by_username(&mut self, username: &str) {
        self.sessions.retain(|_k, v| v.user.to_lowercase() != username.to_lowercase());
    }

    // fn disconnect_client_by_username(&mut self, username: &str) {
        // for (id, session) in &self.sessions {
        //     if session.user == username {
        //          session.recipient.do_send(Disconnect { id: *id });
        //     }
        // }
    // }
}

impl Actor for ChatServer {
    type Context = Context<Self>;
}

impl Handler<Connect> for ChatServer {
    type Result = usize;

    fn handle(&mut self, message: Connect, context: &mut Context<Self>) -> Self::Result {
        println!("Someone joined");

        // register session with random id
        let id = self.rng.gen::<usize>();
        let new_session = Session {
            recipient: message.address,
            user: message.user};
        self.sessions.insert(id, new_session);

        //replay log
        for message in &self.log {
            self.send_message_to_one(&serde_json::to_string(&message).unwrap(), id);
        }

        context.run_interval(HEARTBEAT_INTERVAL*2, |actor, _context| {

            let stream_info = &actor.stream_info;

            let user_count = actor.sessions.len();

            let title = stream_info.title.read().unwrap();
            let game = stream_info.game.read().unwrap();

            let meta_message = messages::Envelope::new(messages::MetaMessage{
                users: user_count,
                title: title.clone(),
                game: game.clone()}, None);

            actor.send_message_to_all(&serde_json::to_string(&meta_message).unwrap());
        });

        // send id back
        id
    }
}

impl Handler<Disconnect> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: Disconnect, _: &mut Context<Self>) {
        println!("Someone disconnected");
        // remove address
        self.sessions.remove(&message.id);
    }
}

impl Handler<messages::Envelope<messages::PlayingMessage>> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::PlayingMessage>, _: &mut Context<Self>) {
        let connection = self.pool.get().expect("couldn't get db connection from pool");

        if let Some(username) = message.user {
            match has_mod_powers(&username, &connection) {
                Ok(Some(true)) =>  {
                    let mut game = self.stream_info.game.write().unwrap();
                    *game = message.contents.game;
                }
                Ok(Some(false)) =>  {
                    self.send_system_message("You do not have permission to set the game.", &username);
                }

                _ => {
                    println!("Error getting user.");
                }
            }
        }
    }
}

impl Handler<messages::Envelope<messages::TitleMessage>> for ChatServer {

    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::TitleMessage>, _: &mut Context<Self>) {
        let connection = self.pool.get().expect("couldn't get db connection from pool");

        if let Some(username) = message.user {
            match has_mod_powers(&username, &connection) {
                Ok(Some(true)) =>  {
                    let mut title = self.stream_info.title.write().unwrap();
                    *title = message.contents.title;
                }
                Ok(Some(false)) =>  {
                    self.send_system_message("You do not have permission to set the title.", &username);
                }
                _ => {
                    println!("Error getting user.");
                }
            }
        }
    }
}

impl Handler<messages::Envelope<messages::PurgeMessage>> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::PurgeMessage>, _: &mut Context<Self>) {
        let connection = self.pool.get().expect("couldn't get db connection from pool");

        if let Some(username) = message.user {
            let target_name = message.contents.target_name;
            let user_mod_powers = has_mod_powers(&username, &connection );

            if username == target_name || user_mod_powers == Ok(Some(true)) {

                let messages = self.find_user_messages(&target_name);

                //Delete from server
                self.delete_user_messages(&target_name);

                //Delete from clients
                for message_id in messages {
                    let message = messages::Envelope::new(
                        messages::DeleteMessage{target_id: message_id},
                        Some(username.clone()));
                    self.send_message_to_all(&serde_json::to_string(&message).unwrap());
                }
            }
            else if user_mod_powers == Ok(Some(false)) {
                self.send_system_message("You do not have permission to purge another user's messages.", &username);
            }
            else {
                print!("Error looking up user's mod powers.");
            }
        }
    }
}


impl Handler<messages::Envelope<messages::ModMessage>> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::ModMessage> , _: &mut Context<Self>) {

        let connection = self.pool.get().expect("couldn't get db connection from pool");
        if let Some(username) = message.user {
            if let Ok(Some(true)) =  has_stream_powers(&username, &connection){
                let target = &message.contents.target_name;

                match mod_user(target, &connection) {
                    Ok(_) =>  {
                        let message = format!("Gave mod powers to {}", target);
                        self.send_system_message(&message, &username);
                        let target_message = format!("Moderator powers granted by {}. With great power, etc., etc.", username);
                        self.send_system_message(&target_message, &target);
                    },
                    Err(_) => {
                        self.send_system_message("Database error.", &username);
                    }
                }
            }
            else {
                self.send_system_message("You do not have permission to mod a user.", &username);
            }
        }
    }
}

impl Handler<messages::Envelope<messages::DemodMessage>> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::DemodMessage> , _: &mut Context<Self>) {
        let connection = self.pool.get().expect("couldn't get db connection from pool");

        if let Some(username) = message.user {
            if let Ok(Some(true)) =  has_stream_powers(&username, &connection){
                let target = &message.contents.target_name;

                match mod_user(target, &connection) {
                    Ok(_) =>  {
                        let target = &message.contents.target_name;
                        let _result = demod_user(target, &connection);

                        let message = format!("Removed mod powers from {}", target);
                        self.send_system_message(&message, &username);
                        let target_message = format!("Moderator powers removed by {}", username);
                        self.send_system_message(&target_message, &target);
                    }
                    Err(_) => {
                    }
                }
            }
            else {
                self.send_system_message("You do not have permission to demod a user.", &username);
            }
        }
    }
}


impl Handler<messages::Envelope<messages::BanMessage>> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::BanMessage> , _: &mut Context<Self>) {
        let connection = self.pool.get().expect("couldn't get db connection from pool");

        if let Some(username) = message.user {
            let target = &message.contents.target_name;
            let user_mod_powers = has_mod_powers(&username, &connection);
            let target_mod_powers = has_mod_powers(&target, &connection);

            if user_mod_powers == Ok(Some(true)) && target_mod_powers == Ok(Some(false)) {

                match ban_user(target, &connection) {
                    Ok(_) =>  {
                        self.send_system_message(&format!("Banned user {}.", &target), &username);

                        //Disconnect the user:
                        self.remove_client_by_username(&target);

                        //TODO should also log user out.
                    }
                    Err(_) => {
                        self.send_system_message(&format!("Banning user {} failed.", &target), &username);
                    }
                }
            }
            else if user_mod_powers == Ok(Some(false)) {
                self.send_system_message("You don't have permission to ban users.", &username)
            }
            else if target_mod_powers == Ok(Some(true)) {
                self.send_system_message("Cannot ban moderator—remove their mod permissions first.", &username)
            }
        }
    }
}


impl Handler<messages::Envelope<messages::UnbanMessage>> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::UnbanMessage> , _: &mut Context<Self>) {
        let connection = self.pool.get().expect("couldn't get db connection from pool");

        if let Some(username) = message.user {
            if let Ok(Some(true)) =  has_mod_powers(&username, &connection){
                let target = &message.contents.target_name;
                let target_user = get_user_by_name(&target, &connection);

                match unban_user(&target, &connection) {
                    Ok(_) => {
                        self.send_system_message(&format!("Unbanned user {}.", &target), &username);
                        //A banned user isn't connected, so we can't message them.
                        send_unban_email(target_user.unwrap());
                    }
                    Err(_) => {
                        self.send_system_message(&format!("Unbanning user {} failed.", &target), &username);
                    }
                }
            }
        }
    }
}


impl Handler<messages::Envelope<messages::HelpMessage>> for ChatServer {
    type Result = ();


    fn handle(&mut self, message: messages::Envelope<messages::HelpMessage> , _: &mut Context<Self>) {

        let connection = self.pool.get().expect("couldn't get db connection from pool");

        let mut help_message: String = "Available chat commands:
                                        /help - display this help message".to_owned();
        let mod_message = "\n/purge USER - deletes all of USER's messages
                           /title TITLE - sets the stream title to TITLE
                           /ban USER - bans USER
                           /unban USER - lifts ban on USER";
        let streamer_message = "\n/mod USER - makes USER a mod
                            /demod USER - removes mod permissions from USER";

        if let Some(username) = message.user {
            if let Ok(Some(true)) =  has_mod_powers(&username, &connection){
                help_message.push_str(&mod_message);
            }
            if let Ok(Some(true)) =  has_stream_powers(&username, &connection){
                help_message.push_str(&streamer_message);
            }
            self.send_system_message(&help_message, &username);
        }
    }
}

pub async fn chat_route(request: HttpRequest, stream: web::Payload,srv: web::Data<Addr<ChatServer>>,id: Identity,) -> Result<HttpResponse, Error> {
    if let Some(id) = id.identity(){
        ws::start(
            sessions::WsChatSession {
                id: 0,
                heartbeat: Instant::now(),
                name: id,
                address: srv.get_ref().clone(),
                rng: rand::thread_rng(),
            },
            &request,
            stream,
        )
    }
    else{
        Err(actix_http::error::ErrorForbidden("Not logged in"))
    }
}

impl Handler<messages::Envelope<messages::ChatMessage>> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::ChatMessage>, _: &mut Context<Self>) {
        let connection = self.pool.get().expect("couldn't get db connection from pool");

        if let Some(username) = &message.user {
            if let Ok(Some(false)) = is_banned(&username, &connection) {
                self.send_message_to_all(&serde_json::to_string(&message).unwrap());
            }
        }

        self.log.push_back(message);
        //Ensure we keep the log the correct size.
        if self.log.len() > MAX_LOG_SIZE {
            self.log.pop_front();
        }
    }
}

impl Handler<messages::Envelope<messages::ServerMessage>> for ChatServer {
    type Result = ();

    fn handle(&mut self, message: messages::Envelope<messages::ServerMessage>, _: &mut Context<Self>) {
        self.send_system_message(&message.contents.message, &message.user.unwrap());
    }
}
