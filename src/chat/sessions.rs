use actix::prelude::*;
use actix_http::*;
use actix_web_actors::ws;
use rand::{self, rngs::ThreadRng};
use std::env;
use std::time::Instant;

use crate::chat::server;
use crate::messages;

pub struct WsChatSession {
    /// unique session id
    pub id: usize,
    /// Client must send ping at least once per 10 seconds (CLIENT_TIMEOUT),
    /// otherwise we drop connection.
    pub heartbeat: Instant,
    /// peer name
    pub name: String,
    /// Chat server
    pub address: Addr<server::ChatServer>,
    pub rng: ThreadRng,
}

impl Actor for WsChatSession {
    type Context = ws::WebsocketContext<Self>;

    /// Method is called on actor start.
    /// We register ws session with ChatServer
    fn started(&mut self, context: &mut Self::Context) {
        // we'll start heartbeat process on session start.
        self.heartbeat(context);

        // register self in chat server. `AsyncContext::wait` register
        // future within context, but context waits until this future resolves
        // before processing any other events.
        // HttpContext::state() is instance of WsChatSessionState, state is shared
        // across all routes within application
        let address = context.address();
        self.address
            .send(server::Connect {
                address: address.recipient(),
                user: self.name.clone()
            })
            .into_actor(self)
            .then(|response, actor, context| {
                match response {
                    Ok(response) => actor.id = response,
                    // something is wrong with chat server
                    _ => context.stop(),
                }
                fut::ready(())
            })
            .wait(context);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        // notify chat server
        self.address.do_send(server::Disconnect { id: self.id });
        Running::Stop
    }
}

impl Handler<server::Message> for WsChatSession {
    type Result = ();

    fn handle(&mut self, message: server::Message, context: &mut Self::Context) {
        context.text(message.0);
    }
}


impl Handler<server::Disconnect> for WsChatSession {
    type Result = ();

    fn handle(&mut self, _message: server::Disconnect, context: &mut Self::Context) {
        // stop actor
        context.stop();

        // don't try to send a ping
        return;
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsChatSession {
    fn handle(
        &mut self,
        message: Result<ws::Message, ws::ProtocolError>,
        context: &mut Self::Context,
    ) {
        let message_limit = match env::var("MAX_MESSAGE_LENGTH") {
            Ok(val) => val.parse::<usize>().unwrap_or(750),
            Err(_) => 750,
        };

        let message = match message {
            Err(_) => {
                context.stop();
                return;
            }
            Ok(message) => message,
        };

        println!("WEBSOCKET MESSAGE: {:?}", message);

        match message {
            ws::Message::Ping(message) => {
                self.heartbeat = Instant::now();
                context.pong(&message);
            }
            ws::Message::Pong(_) => {
                self.heartbeat = Instant::now();
            }
            ws::Message::Text(text) => {
                let m = text.trim();

                if m.len() > message_limit {
                    self.send_system_message(&format!("Message must be {} characters or less", message_limit));
                }
                // we check for /sss type of messages
                else if m.starts_with('/') {
                    let v: Vec<&str> = m.splitn(2, ' ').collect();
                    match v[0] {
                        "/playing" => {
                            if v.len() == 2 {
                                self.address.do_send(messages::Envelope::new(messages::PlayingMessage{game: Some(v[1].to_string())},
                                Some(self.name.to_owned())));

                            } else {
                                context.text("!!! a game title is required");
                                self.send_system_message("Game title is required for /playing.")
                            }
                        }
                        "/title" => {
                            if v.len() == 2 {
                                let message = messages::Envelope::new(messages::TitleMessage{title: Some(v[1].to_string())},
                                Some(self.name.to_owned()));
                                self.address.do_send(message);
                            } else {
                                context.text("!!! a title is required");
                                self.send_system_message("Title is required for /title.")

                            }
                        }
                        "/purge" => {
                            if v.len() == 2 {
                                let message = messages::Envelope::new(
                                        messages::PurgeMessage{target_name: v[1].to_owned()},
                                        Some(self.name.to_owned()));
                                self.address.do_send(message);
                            } else {
                                context.text("!!! message id is required");
                                self.send_system_message("Username is required for /purge.")
                            }
                        }
                        "/mod" => {
                            if v.len() == 2 {
                                let message = messages::Envelope::new(
                                    messages::ModMessage{target_name: v[1].to_string() },
                                    Some(self.name.to_owned())
                                    );
                                self.address.do_send(message)
                            } else {
                                context.text("!!! username is required");
                                self.send_system_message("Username is required for /mod.")
                            }
                        }
                        "/demod" => {

                            if v.len() == 2 {
                                let message = messages::Envelope::new(
                                    messages::DemodMessage{target_name: v[1].to_string() },
                                    Some(self.name.to_owned()));
                                self.address.do_send(message)
                            } else {
                                context.text("!!! username is required");
                                self.send_system_message("Username is required for /demod.")
                            }
                        }
                        "/ban" => {
                            if v.len() == 2 {
                                let message = messages::Envelope::new(
                                    messages::BanMessage{target_name: v[1].to_string() },
                                    Some(self.name.to_owned()));
                                self.address.do_send(message)
                            } else {
                                context.text("!!! username is required");
                                self.send_system_message("Username is required for /ban.")
                            }
                        }
                        "/unban" => {
                            if v.len() == 2 {
                                let message = messages::Envelope::new(
                                    messages::UnbanMessage{target_name: v[1].to_string() },
                                    Some(self.name.to_owned()));
                                self.address.do_send(message)
                            } else {
                                context.text("!!! username is required");
                                self.send_system_message("Username is required for /unban.")
                            }
                        }
                        "/help" => {
                            let message = messages::Envelope::new(
                                messages::HelpMessage{},
                                Some(self.name.to_owned()));
                            self.address.do_send(message);
                        }
                        _ => {
                            context.text(format!("!!! unknown command: {:?}", m));
                            self.send_system_message(&format!("Unknown command {:?}", m));
                        }
                    }
                } else {
                    // send message to chat server
                    self.address.do_send(messages::Envelope::new(
                            messages::ChatMessage{message: m.to_string()},
                            Some(self.name.to_owned())));
                }
            }
            ws::Message::Binary(_) => println!("Unexpected binary"),
            ws::Message::Close(reason) => {
                context.close(reason);
                context.stop();
            }
            ws::Message::Continuation(_) => {
                context.stop();
            }
            ws::Message::Nop => (),
        }
    }
}

impl WsChatSession {
    /// helper method that sends ping to client every second.
    ///
    /// also this method checks heartbeats from client
    fn heartbeat(&self, context: &mut ws::WebsocketContext<Self>) {
        context.run_interval(server::HEARTBEAT_INTERVAL, |actor, context| {
            // check client heartbeats
            if Instant::now().duration_since(actor.heartbeat) > server::CLIENT_TIMEOUT {
                // heartbeat timed out
                println!("Websocket Client heartbeat failed, disconnecting!");

                // notify chat server
                actor.address.do_send(server::Disconnect { id: actor.id });

                // stop actor
                context.stop();

                // don't try to send a ping
                return;
            }

            context.ping(b"");
        });
    }

    fn send_system_message(&self, message: &str) {
    ////Create a notification message and send it to the chat server.
    ////Eventually processed by server::send_notification, btw.

        let server_message = messages::Envelope::new(
            messages::ServerMessage{recipient: self.name.to_string() ,
            message: message.to_string()},
            Some(self.name.to_owned()));
        self.address.do_send(server_message);

    }
}
