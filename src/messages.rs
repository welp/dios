use actix::Message;
use serde::{Serialize, Deserialize};
use rand::Rng;
use std::time::{SystemTime, UNIX_EPOCH};

use dios_derive::SerializableType;

pub trait SerializableType {
    fn get_type() -> String;
}

//Envelope essentially carries metadata shared between message types.
//however, we don't actually send it to the client. instead, it gets
//converted to SerializableEnvelope, thanks to the magic of Serde.
//The resulting JSON is flat and includes the message type.
//The type is basically "stored" in the get_type method, which is implemented for us
//by a derive proc macro.

#[derive(actix::Message, Serialize, Deserialize, Clone)]
#[rtype(result = "()")]
#[serde(into = "SerializableEnvelope<T>")]
pub struct Envelope<T: SerializableType + Clone> {
    pub message_id: u32,
    pub time: u128,
    pub user: Option<String>,
    #[serde(flatten)]
    pub contents: T
}

//Exists only for Message to be serialized into
#[derive(Serialize, Deserialize, Clone)]
pub struct SerializableEnvelope<T> {
    message_id: u32,
    time: u128,
    user: Option<String>,
    message_type: String,
    #[serde(flatten)]
    contents: T
}

impl<T: SerializableType + Clone> Envelope<T> {
    pub fn new(contents: T, user: Option<String>) -> Envelope<T> {
        let mut rng = rand::thread_rng();
        Envelope::<T> {
            message_id: rng.gen::<u32>(),
            time: SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis(),
            user: user,
            contents: contents
        }
    }
}

impl<T: SerializableType + Clone> From<Envelope<T>> for SerializableEnvelope<T> {
    fn from(orig: Envelope<T>) -> SerializableEnvelope::<T> {
        SerializableEnvelope::<T> {
            message_id: orig.message_id,
            time: orig.time,
            user: orig.user,
            message_type: T::get_type(),
            contents: orig.contents
        }
    }
}

// SPECIFIC MESSAGES
//
#[derive(actix::Message, Serialize, Deserialize, Clone, SerializableType)]
#[rtype(result = "()")]
pub struct PlayingMessage {
    pub game: Option<String>
}

#[derive(actix::Message, Serialize, Deserialize, Clone, SerializableType)]
#[rtype(result = "()")]
pub struct TitleMessage {
    pub title: Option<String>
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct MetaMessage {
    pub users: usize,
    pub title: Option<String>,
    pub game: Option<String>//,
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct DeleteMessage {
    pub target_id: u32
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct PurgeMessage {
    pub target_name: String
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct BanMessage {
    pub target_name: String
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct UnbanMessage {
    pub target_name: String
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct ChatMessage {
    pub message: String
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct ServerMessage {
    pub message: String,
    pub recipient: String
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct ModMessage {
    pub target_name: String
}

#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct DemodMessage {
    pub target_name: String
}


#[derive(Message, Clone, Serialize, Deserialize, SerializableType)]
#[rtype(result = "()")]
pub struct HelpMessage{}
