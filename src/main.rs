#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate argon2;
extern crate actix_files;

use actix::*;
use actix_identity::*;
use actix_files::Files;
use actix_session::{CookieSession};
use actix_web::{web, middleware, App, HttpServer};
use dotenv::dotenv;
use env_logger;
use listenfd::ListenFd;
use std::env;
use std::path::Path;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
#[macro_use] extern crate diesel_migrations;
embed_migrations!();

pub mod messages;
pub mod models;
pub mod schema;
pub mod stream_info;
pub mod templates;

pub mod chat {
    pub mod server;
    pub mod sessions;
}

pub mod routes {
    pub mod auth;
    pub mod public;
}

pub mod utils {
    pub mod mail;
    pub mod user;
}

type ConnectionPool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or("info")
    ).init();

    // HOST and PORT set the bind address
    let host_env = "HOST";
    let host = match env::var(host_env) {
        Ok(val) => val,
        Err(_) => String::from("localhost"),
    };
    let port_env = "PORT";
    let port = match env::var(port_env) {
        Ok(val) => val,
        Err(_) => String::from("8080"),
    };
    let bind_address = format!("{}:{}", host, port);

    // where to look for the stream manifests created by nginx-rtmp
    let streamfs_env = "STREAMFS";
    let streamfs = match env::var(streamfs_env) {
        Ok(val) => val,
        Err(_) => String::from("tmp"),
    };

    // for whether or not to use secure cookies
    let is_https = match env::var("HTTPS") {
        Ok(val) => val == "true" || val == "1" ,
        Err(_) => false,
    };

    let pg_db = env::var("POSTGRES_DB").expect("POSTGRES_DB unset");
    let pg_host = env::var("POSTGRES_HOST").expect("POSTGRES_HOST unset");
    let pg_pass = env::var("POSTGRES_PASSWORD").expect("POSTGRES_PASSWORD unset");
    let pg_user = env::var("POSTGRES_USER").expect("POSTGRES_USER unset");
    let database_url = format!("postgres://{}:{}@{}/{}",
                               &pg_user,
                               &pg_pass,
                               &pg_host,
                               &pg_db,);

    let manager = ConnectionManager::<PgConnection>::new(&database_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    let conn = pool.get().expect("couldn't get db connection from pool");
    embedded_migrations::run_with_output(
        &conn,
        &mut std::io::stdout()
    ).expect("Failed to run migrations");


    let server_state = web::Data::new(stream_info::StreamInfo::default());

    //for now, use a separate pool for the ChatServer
    let server = chat::server::ChatServer::new(server_state.clone(), pool.clone()).start();

    let mut server = HttpServer::new(move || {
        App::new()
            .data(pool.clone())
            .data(server.clone())
            .app_data(server_state.clone())
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&env::var("AUTH_KEY").expect("AUTH_KEY must be set").as_bytes())
                    .name("auth-cookie")
                    .secure(is_https)
            ))
            .wrap(CookieSession::signed(&env::var("SESSION_KEY").expect("SESSION_KEY must be set").as_bytes()).secure(is_https))
            .wrap(middleware::Logger::default())
            .service(Files::new("/hls/", Path::new(&streamfs).join("hls/")))
            .service(Files::new("/static/", "static/"))
            .service(web::resource("/ws/").to(chat::server::chat_route))
            .service(routes::auth::activate)
            .service(routes::auth::change_username)
            .service(routes::auth::login)
            .service(routes::auth::logout)
            .service(routes::auth::new_password)
            .service(routes::auth::new_password_callback)
            .service(routes::auth::on_publish)
            .service(routes::auth::profile)
            .service(routes::auth::reset)
            .service(routes::auth::signup)
            .service(routes::public::chat)
            .service(routes::public::index)
            .service(routes::public::reset)
            .service(routes::public::video)
    });

    // https://actix.rs/docs/autoreload/
    let mut listenfd = ListenFd::from_env();
    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind(bind_address)?
    };

    server.run().await
}
