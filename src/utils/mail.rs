use lettre::{transport::smtp::authentication::Credentials, Message, SmtpTransport, Transport};
use std::env;

use crate::models::*;

fn make_mailer () -> SmtpTransport {
    let smtp_host = env::var("SMTP_HOST").expect("SMTP_HOST unset");
    let smtp_pass = env::var("SMTP_PASS").expect("SMTP_PASS unset");
    let smtp_user = env::var("SMTP_USER").expect("SMTP_USER unset");
    let smtp_port = match env::var("SMTP_PORT") {
        Ok(val) => val.parse::<u16>().unwrap_or(587),
        Err(_) => 587,
    };
    let creds = Credentials::new(smtp_user, smtp_pass);

    SmtpTransport::starttls_relay(&smtp_host)
        .unwrap()
        .credentials(creds)
        .port(smtp_port)
        .build()
}

pub fn send_confirmation_email (confirmation: Confirmation) -> () {
    let app_name = env::var("APP_TITLE").expect("APP_TITLE unset");
    let domain = env::var("APP_DOMAIN").expect("APP_DOMAIN unset");
    let smtp_from = env::var("SMTP_FROM").expect("SMTP_FROM unset");

    let body = format!(
        "hello please click the link below to activate your account on {}:\n\nhttps://{}/activate?code={}\n\nthis link will expire in 24 hours",
        &app_name,
        domain,
        confirmation.id,
    );

    let pigeon = Message::builder()
        .from(smtp_from.parse().unwrap())
        .to(confirmation.email.parse().unwrap())
        .subject(format!("welcome to {}", &app_name))
        .body(body)
        .unwrap();

    match make_mailer().send(&pigeon) {
        Ok(_) => println!("email sent to {}", confirmation.email),
        Err(e) => panic!("Could not send email: {:?}", e),
    }
}

pub fn send_reset_email (reset: PasswordReset) -> () {
    let app_name = env::var("APP_TITLE").expect("APP_TITLE unset");
    let domain = env::var("APP_DOMAIN").expect("APP_DOMAIN unset");
    let smtp_from = env::var("SMTP_FROM").expect("SMTP_FROM unset");

    let body = format!(
        "click the link below to reset your password on {}:\n\nhttps://{}/new_password?code={}\n\nthis link will expire in 1 hour",
        &app_name,
        domain,
        reset.id,
    );

    let pigeon = Message::builder()
        .from(smtp_from.parse().unwrap())
        .to(reset.email.parse().unwrap())
        .subject(format!("password reset for {}", &app_name))
        .body(body)
        .unwrap();

    match make_mailer().send(&pigeon) {
        Ok(_) => println!("email sent to {}", reset.email),
        Err(e) => panic!("Could not send email: {:?}", e),
    }
}

pub fn send_unban_email (unbanned_user: User) -> () {
    let app_name = env::var("APP_TITLE").expect("APP_TITLE unset");
    let domain = env::var("APP_DOMAIN").expect("APP_DOMAIN unset");
    let smtp_from = env::var("SMTP_FROM").expect("SMTP_FROM unset");

    let body = format!(
        "hello {}. your ban on {} has been lifted. welcome back!\n\n{}",
        unbanned_user.username,
        &app_name,
        domain
        );

    let pigeon = Message::builder()
        .from(smtp_from.parse().unwrap())
        .to(unbanned_user.email.parse().unwrap())
        .subject(format!("unbanned from {}", &app_name))
        .body(body)
        .unwrap();

    match make_mailer().send(&pigeon) {
        Ok(_) => println!("email sent to {}", unbanned_user.email),
        Err(e) => panic!("Could not send email: {:?}", e)
    }
}
