use tera::Tera;
use tera::Context;

lazy_static! {
    pub static ref TEMPLATES: Tera = {
        let mut tera = match Tera::new("templates/**/*.tera") {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                ::std::process::exit(1);
            }
        };
        tera.autoescape_on(vec!["html", ".sql"]);
        tera
    };
}


pub fn render(template: &str, context: &Context) -> String {
    match TEMPLATES.render(template, context) {
        Ok(t) => t,
        Err(e) =>  format!("Error: {}", e)
    }
}
