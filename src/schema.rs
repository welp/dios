table! {
    confirmations (id) {
        id -> Varchar,
        email -> Varchar,
        expires_at -> Timestamp,
    }
}

table! {
    password_resets (id) {
        id -> Varchar,
        email -> Varchar,
        expires_at -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Varchar,
        username -> Varchar,
        password_hash -> Varchar,
        moderator -> Bool,
        streamer -> Bool,
        email -> Varchar,
        activated -> Bool,
        indefinitely_banned -> Bool,
        last_namechange -> Timestamp,
    }
}

allow_tables_to_appear_in_same_query!(
    confirmations,
    password_resets,
    users,
);
