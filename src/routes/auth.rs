use actix_web::{web, get, post, HttpRequest, HttpResponse, Error};
use actix_http::http;
use actix_identity::*;
use actix_session::Session;
use argon2::{self, Config};
use diesel::prelude::*;
use rand::prelude::*;
use serde::Deserialize;
use std::env;
use tera::Context;

use crate::ConnectionPool;
use crate::models::*;
use crate::schema::confirmations;
use crate::schema::password_resets;
use crate::schema::users::dsl::*;
use crate::templates;
use crate::utils::mail::*;
use crate::utils::user::*;

#[derive(Deserialize)]
pub struct LoginForm<>{
    chatonly: String,
    email: String,
    password: String,
}

#[derive(Deserialize)]
pub struct SignupForm<>{
    chatonly: String,
    email: String,
    password: String,
    username: String,
}

#[derive(Deserialize)]
pub struct LogoutForm<>{
    chatonly: String,
}

#[derive(Deserialize)]
pub struct ResetForm<>{
    email: String,
}

#[derive(Deserialize)]
pub struct NewPasswordForm<>{
    reset_code: String,
    password: String,
    password_conf: String,
}

#[derive(Deserialize)]
pub struct NewUsernameForm<>{
    username: String,
}

// form params https://github.com/arut/nginx-rtmp-module/wiki/Directives#on_play
#[derive(Deserialize)]
pub struct RtmpPostData<>{
    name: String,
}

#[post("/signup")]
pub async fn signup(
    form: web::Form<SignupForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error>{
    let connection = pool.get().expect("couldn't get db connection from pool");

    let mut redirect_location = "/";
    if form.chatonly == "true" {
        redirect_location = "/chat";
    };

    let cleaned_user: String = form.username
        .chars()
        .filter(|c| is_allowed_char(c) )
        .collect();

    let username_limit = match env::var("MAX_USERNAME_LENGTH") {
        Ok(val) => val.parse::<usize>().unwrap_or(20),
        Err(_) => 20,
    };

    if cleaned_user.len() > username_limit {
        session.set(
            "error",
            format!("username must be {} characters or less", username_limit)
        ).expect("failed to flash ;_;");

        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, redirect_location)
                  .finish())
    }

    match get_user_by_email(&form.email, &connection) {
        // db query success
        Ok(Some(found_user)) => signup_existing_user(found_user, &connection, &session),
        Ok(None) => {
            signup_new_user(
                &cleaned_user,
                &form.email,
                &form.password,
                &connection,
                &session,
                &identity,
            )
        },
        Err(e) => panic!("Failed to query DB for email {}: {:?}", &form.email, e)
    }
    Ok(HttpResponse::SeeOther()
       .set_header(http::header::LOCATION, redirect_location)
       .finish())

}

#[get("/activate")]
pub async fn activate(
    req: HttpRequest,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error> {
    let connection = pool.get().expect("couldn't get db connection from pool");

    let query: Vec<&str> = req.query_string().split('=').collect();
    let querycode = query.get(1);
    if querycode == None {
        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, "/")
                  .finish())
    }
    let confirmation_code = querycode.unwrap();

    let find_conf = confirmations::table
        .filter(confirmations::id.eq(confirmation_code))
        .first::<Confirmation>(&connection)
        .optional()
        .unwrap();

    if let Some(found_conf) = find_conf {
        let now = chrono::Utc::now().naive_utc();

        if found_conf.expires_at > now {
            diesel::update(users.filter(email.eq(&found_conf.email)))
                .set(activated.eq(true))
                .execute(&connection)
                .unwrap();

            diesel::update(
                confirmations::table
                    .filter(confirmations::id.eq(confirmation_code)))
                .set(confirmations::expires_at.eq(now))
                .execute(&connection)
                .unwrap();

            let find_user = users
                .filter(email.eq(&found_conf.email))
                .first::<User>(&connection)
                .optional()
                .unwrap();

            if let Some(found_user) = find_user {
                identity.remember(found_user.username.to_owned());
            } else {
                session.set("error", "no user found with this email address").expect("failed to flash ;_;");
            }
        } else {
            session.set("error", "invalid confirmation code").expect("failed to flash ;_;");
        }
    } else {
        session.set("error", "invalid confirmation code").expect("failed to flash ;_;");
    }

    Ok(HttpResponse::SeeOther()
       .set_header(http::header::LOCATION, "/")
       .finish())
}

#[post("/login")]
pub async fn login(
    form: web::Form<LoginForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>
) -> Result<HttpResponse, Error>{
    let connection = pool.get().expect("couldn't get db connection from pool");

    let password = form.password.to_owned();

    let mut redirect_location = "/";
    if form.chatonly == "true" {
        redirect_location = "/chat";
    };

    match get_user_by_email(&form.email, &connection) {
        Ok(Some(found_user)) => {
            if ! &found_user.activated {
                let new_confirmation = create_confirmation_for(
                    &found_user.email,
                    &connection,
                );
                send_confirmation_email(new_confirmation);
                session.set("error", "user is not activated; re-sending confirmation email").expect("failed to flash ;_;");
            } else if found_user.indefinitely_banned {
                session.set("error", "no user exists with that email address.").expect("failed to flash ;_;");

            } else if argon2::verify_encoded(&found_user.password_hash, password.as_bytes()).unwrap() {
                identity.remember(found_user.username.to_owned());
            } else {
                session.set("error", "incorrect password.").expect("failed to flash ;_;");
            }
        },
        Ok(None) => session.set("error", "no user exists with that email address.").expect("failed to flash ;_;"),
        Err(e) => panic!("Failed to query DB for email {}: {:?}", &form.email, e),
    }
    Ok(HttpResponse::SeeOther()
       .set_header(http::header::LOCATION, redirect_location)
       .finish())
}

#[post("/logout")]
pub async fn logout(form: web::Form<LogoutForm>, identity: Identity) -> HttpResponse {
    let mut redirect_location = "/";
    if form.chatonly == "true" {
        redirect_location = "/chat";
    };

    identity.forget();
    HttpResponse::SeeOther()
        .set_header(http::header::LOCATION, redirect_location)
        .finish()
}

#[post("/on_publish")]
pub async fn on_publish(form: web::Form<RtmpPostData>) -> HttpResponse {
    match env::var("STREAM_KEY") {
        Ok(key) => {
            if form.name == key {
                // a redirect is required to tell nginx-rtmp wha to name the
                // stream manifest (otherwise it will use the stream key as the
                // filename)
                HttpResponse::SeeOther()
                    // this has to match the name of the rtmp application
                    // defined in the NGINX config
                    .set_header(http::header::LOCATION, "/stream")
                    .finish()
            } else {
                HttpResponse::Forbidden().finish()
            }
        },
        Err(_) => {
            HttpResponse::BadRequest().finish()
        }
    }
}

#[post("/reset")]
pub async fn reset(
    form: web::Form<ResetForm>,
    _identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error> {
    let connection = pool.get().expect("couldn't get db connection from pool");

    match get_user_by_email(&form.email, &connection) {
        Ok(Some(found_user)) => {
            let reset = create_reset_for(
                &found_user.email,
                &connection,
            );
            send_reset_email(reset);
            session.set("error", "password reset email sent; valid for 1 hour").expect("failed to flash ;_;");
        },
        Ok(None) => session.set("error", "no user exists with that email address.").expect("failed to flash ;_;"),
        Err(e) => panic!("Failed to query DB for email {}: {:?}", &form.email, e),
    }

    Ok(HttpResponse::SeeOther()
       .set_header(http::header::LOCATION, "/")
       .finish())
}

#[get("/new_password")]
pub async fn new_password(
    req: HttpRequest,
    _identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error> {
    let connection = pool.get().expect("couldn't get db connection from pool");

    let query: Vec<&str> = req.query_string().split('=').collect();
    let querycode = query.get(1);
    if querycode == None {
        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, "/")
                  .finish())
    }
    let reset_code = querycode.unwrap();

    let find_reset = password_resets::table
        .filter(password_resets::id.eq(reset_code))
        .first::<PasswordReset>(&connection)
        .optional()
        .unwrap();

    if let Some(found_reset) = find_reset {
        if found_reset.expires_at > chrono::Utc::now().naive_utc() {
            let mut context = Context::new();
            context.insert("reset_code", &found_reset.id);
            context.insert("email", &found_reset.email);
            context.insert("page_title", "reset password");

            return Ok(HttpResponse::Ok().body(templates::render("new_password.html.tera", &context)))
        } else {
            session.set("error", "password reset code is expired").expect("failed to flash ;_;");
            return Ok(HttpResponse::SeeOther()
                      .set_header(http::header::LOCATION, "/reset")
                      .finish())
        }
    } else {
        session.set("error", "invalid password reset code").expect("failed to flash ;_;");
        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, "/reset")
                  .finish())
    }
}

#[post("/new_password")]
pub async fn new_password_callback(
    form: web::Form<NewPasswordForm>,
    _identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error> {
    let pw = &form.password;
    let pw_chk = &form.password_conf;
    let connection = pool.get().expect("couldn't get db connection from pool");

    let reset_code = &form.reset_code;
    let find_reset = password_resets::table
        .filter(password_resets::id.eq(reset_code))
        .first::<PasswordReset>(&connection)
        .optional()
        .unwrap();

    if let Some(found_reset) = find_reset {
        let now = chrono::Utc::now().naive_utc();

        if found_reset.expires_at > now {
            if pw != pw_chk {
                session.set("error", "passwords do not match").expect("failed to flash ;_;");
                let mut context = Context::new();
                context.insert("reset_code", &found_reset.id);
                context.insert("email", &found_reset.email);
                return Ok(HttpResponse::Ok().body(templates::render("new_password.html.tera", &context)))
            } else {
                let salt: u64 = random();
                let config = Config::default();

                let hashed_pw = argon2::hash_encoded(&pw.as_bytes(), &salt.to_be_bytes(), &config).unwrap();
                diesel::update(users.filter(email.eq(&found_reset.email)))
                    .set(password_hash.eq(&hashed_pw))
                    .execute(&connection)
                    .unwrap();

                diesel::update(
                    password_resets::table
                        .filter(password_resets::id.eq(reset_code)))
                    .set(password_resets::expires_at.eq(now))
                    .execute(&connection)
                    .unwrap();

                session.set("error", "new password set").expect("failed to flash ;_;");
                return Ok(HttpResponse::SeeOther()
                          .set_header(http::header::LOCATION, "/")
                          .finish())
            }
        } else {
            session.set("error", "password reset code is expired").expect("failed to flash ;_;");
            return Ok(HttpResponse::SeeOther()
                      .set_header(http::header::LOCATION, "/reset")
                      .finish())
        }
    } else {
        session.set("error", "invalid password reset code").expect("failed to flash ;_;");
        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, "/reset")
                  .finish())
    }
}

#[get("/profile")]
pub async fn profile (
    _req: HttpRequest,
    identity: Identity,
) -> HttpResponse {
    if let Some(iden) = identity.identity() {
        let mut context = Context::new();
        context.insert("user", &iden);
        context.insert("page_title", &format!("profile for {}", &iden));

        return HttpResponse::Ok().body(templates::render("profile.html.tera", &context))
    } else {
        return HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/")
            .finish()
    }
}

#[post("/change_username")]
pub async fn change_username (
    form: web::Form<NewUsernameForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Some(iden) = identity.identity() {
        let connection = pool.get().expect("couldn't get db connection from pool");

        let newname: String = form.username
            .chars()
            .filter(|c| is_allowed_char(c) )
            .collect();

        let mut context = Context::new();
        context.insert("user", &iden);
        context.insert("page_title", &format!("profile for {}", &iden));

        match get_user_by_name(&iden, &connection) {
            Some(u) => {
                let now = chrono::Utc::now().naive_utc();

                if u.last_namechange + chrono::Duration::minutes(5) > now {
                    context.insert(
                        "error",
                        "you can only change your username once every five minutes",
                    );

                    return HttpResponse::SeeOther()
                        .body(templates::render("profile.html.tera", &context))

                } else {
                    match get_user_by_name(&newname, &connection) {
                        Some(_collision) => {
                            context.insert(
                                "error",
                                &format!("the name {} is already in use", &newname)
                            );

                            return HttpResponse::SeeOther()
                                .body(templates::render("profile.html.tera", &context))
                        },
                        None => {
                            diesel::update(users.filter(username.eq(&iden)))
                                .set((
                                    username.eq(&newname),
                                    last_namechange.eq(&now),
                                ))
                                .execute(&connection)
                                .unwrap();

                            identity.remember(String::from(&newname));

                            session.set(
                                "error",
                                &format!("name updated successfully. hello, {}", &newname)
                            ).expect("failed to flash ;_;");

                            return HttpResponse::SeeOther()
                                .set_header(http::header::LOCATION, "/")
                                .finish()
                        },
                    }
                }
            },
            None => {
                session.set(
                    "error",
                    &format!("failed to retrieve entry for '{}'; please contact an administrator", newname)
                ).expect("failed to flash ;_;");

                return HttpResponse::SeeOther()
                    .body(templates::render("profile.html.tera", &context))
            },
        }
    } else {
        return HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/")
            .finish()
    }
}
