use actix_identity::*;
use actix_web::{web, get, HttpResponse, HttpRequest};
use actix_session::Session;
use std::env;
use tera::Context;

use crate::stream_info::StreamInfo;
use crate::templates;

fn get_page_title() -> String {
    let title_env = "APP_TITLE";
    let page_title = match env::var(title_env) {
        Ok(val) => val,
        Err(_) => String::from("DIOS"),
    };

    page_title
}

fn make_page_context(id: Identity, session: Session, stream_info: web::Data<StreamInfo>) -> tera::Context {
    let mut context = Context::new();

    let empty = String::from("");
    let title_op = stream_info.title.read().unwrap();
    let title = title_op.as_ref().unwrap_or(&empty);
    let game_op = stream_info.game.read().unwrap();
    let game = game_op.as_ref().unwrap_or(&empty);

    context.insert("game", &game);
    context.insert("title", &title);
    context.insert("page_title", &get_page_title());

    if let Some(id) = id.identity() {
        context.insert("user", &id);
    }

    if let Some(error) = session.get::<String>("error").unwrap(){
        context.insert("error", &error);
    }

    session.remove("error");

    context
}

#[get("/")]
pub async fn index(_req: HttpRequest, id: Identity, session: Session, _stream: web::Payload,
               stream_info: web::Data<StreamInfo>) -> HttpResponse {
    let context = make_page_context(id, session, stream_info);
    HttpResponse::Ok().body(templates::render("combined.html.tera", &context))
}

#[get("/reset")]
pub async fn reset(_req: HttpRequest) -> HttpResponse {
    let mut context = Context::new();
    context.insert("page_title", &get_page_title());

    HttpResponse::Ok().body(templates::render("reset.html.tera", &context))
}

#[get("/chat")]
pub async fn chat(_req: HttpRequest, id: Identity, session: Session, _stream: web::Payload,
                  stream_info: web::Data<StreamInfo>) -> HttpResponse {
    let mut context = make_page_context(id, session, stream_info);
    context.insert("chatonly", &true);

    HttpResponse::Ok().body(templates::render("chat-only.html.tera", &context))
}

#[get("/video")]
pub async fn video(_req: HttpRequest, _id: Identity, _stream: web::Payload) -> HttpResponse {
    let mut context = Context::new();
    context.insert("page_title", &get_page_title());

    HttpResponse::Ok().body(templates::render("video-only.html.tera", &context))
}
