(function () {
  let conn = null
  let reconnect_time = 1000
  let reconnect_mult = 2
  let colors = {}
  const URL_REGEX = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/g
  const LIGHT_BACKGROUND = "#ffffff"
  const DARK_BACKGROUND = "#000000"
  const LIGHT_TEXT = "#000000"
  const DARK_TEXT = "#ffffff"
  const MIN_CONTRAST = 5;

  function get_username_color() {
    let letters = '0123456789ABCDEF'
    let color = '#'
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)]
    }

    const lights = document.getElementById("toggle-lights").checked

    if (contrast(color, lights ? LIGHT_BACKGROUND : DARK_BACKGROUND) < MIN_CONTRAST) {
      color = get_username_color()
    }

    return color
  }

  function luminance(color) {
    const r = parseInt(color.slice(1, 3), 16)
    const g = parseInt(color.slice(3, 5), 16)
    const b = parseInt(color.slice(5, 7), 16)

    let a = [r, g, b].map(function (v) {
      v /= 255
      return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4)
    });
    return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722
  }

  function contrast(color1, color2) {
    const lum1 = luminance(color1)
    const lum2 = luminance(color2)
    const brightest = Math.max(lum1, lum2)
    const darkest = Math.min(lum1, lum2)

    const contrast = (brightest + 0.05) / (darkest + 0.05)

    return contrast
  }

  function log(msg, user, time, id) {
    if (typeof id === "undefined") {
      id = Math.floor(Math.random() * 10000)
    }
    if (typeof user === "undefined") {
      user = "System"
    }
    if (typeof time === "undefined") {
      time = new Date(Date.now())
    }
    const locale = navigator.languages != undefined ? navigator.languages[0] : navigator.language
    const timestamp = time.toLocaleTimeString(
      locale,
      { timeStyle: "short", }
    )

    const nativeControl = document.querySelector('#log')
    const atBottom = (nativeControl.scrollTop + nativeControl.offsetHeight >= nativeControl.scrollHeight)

    let timeStamp = `<time datetime="${time.toISOString()}" class="chat-timestamp">&lt${timestamp}&gt</time> `
    let userInfo = `<span class="chat-username" style="color:${colors[user]}"></span>: `
    // let userInfo = `<span class="chat-username" style="color:${colors[user]}">${user}</span>: `
    let messageString = `<span id="msg-${id}" class="chat-message-text"></span>`

    nativeControl.innerHTML += `<p class="chat-message" id="${id}">${timeStamp + userInfo + messageString}</p>`

    let paragraphs = msg.split("\n")
    paragraphs.forEach( (msg_para) => { 
      let new_span = document.createElement("span")
      let new_br = document.createElement("br")
      new_span.textContent = msg_para
      document.getElementById(`msg-${id}`).appendChild(new_span) 
      document.getElementById(`msg-${id}`).appendChild(new_br) 
    })

    document.getElementById(`msg-${id}`).textContent = msg;
    document.getElementById(`msg-${id}`).previousElementSibling.textContent = user;

    let urls = document.getElementById(`msg-${id}`).textContent.matchAll(URL_REGEX)
    for (const match of urls) {
      let href
      try {
        new URL(match[0])
        href = match[0]
      } catch (_e) {
        href = `http://${match[0]}`
      }
      document.getElementById(`msg-${id}`).innerHTML = document.getElementById(`msg-${id}`).innerHTML.replace(match[0], `<a href="${href}" target="_blank">${href}</a>`)
    }

    if (atBottom)
      nativeControl.scrollTop = nativeControl.scrollHeight
  }

  function connect() {
    const wsUri = (window.location.protocol == 'https:' && 'wss://' || 'ws://') + window.location.host + '/ws/'
    conn = new WebSocket(wsUri)
    log('Connecting...')
    conn.onopen = function () {
      reconnect_time = 1
      log('Connected.')
      update_ui()
    }
    conn.onmessage = function (e) {
      const message = JSON.parse(e.data)
      if (!(message.user in colors)) {
        colors[message.user] = get_username_color()
      }

      if (message.message_type == 'Chat') {
        log(message.message, message.user, new Date(message.time), message.message_id)
      }
      else if (message.message_type == 'Delete') {
        const elem = document.querySelector(`#msg-${message.target_id}`)

        elem.innerHTML = "<em>deleted</em>"
        elem.classList.add("deleted");
      }
      else if (message.message_type == 'Meta') {
        let elem = document.querySelector('#counter')
        let title_elem = document.querySelector('#title')
        // let game_elem = document.querySelector('#game')

        elem.innerHTML = message.users
        title_elem.innerHTML = message.title
        // game_elem.innerHTML = message.game

      }
      else if (message.message_type == 'Server') {
        //don't love undefined. Maybe pull it out as a constant? TODO
        log(message.message, undefined, new Date(message.time), message.message_id)
      }
      else {
        log("message of type" + message.message_type + "not accounted for.")
      }
      update_ui()
    }
    conn.onclose = function () {
      log('Disconnected.')
      conn = null
      setTimeout(connect(), reconnect_time)
      reconnect_time *= reconnect_mult
      console.log("reconnect time  " + reconnect_time)
      update_ui()
    }

  }


  function update_ui() {
    const status = document.querySelector('#status')

    if (conn == null) {
      status.classList.remove('chat-connected')
      status.classList.add('chat-disconnected')
    } else {
      status.classList.remove('chat-disconnected')
      status.classList.add('chat-connected')
    }
  }

  function toggle_lights() {
    let body = document.getElementsByTagName('body')[0]
    let log = document.getElementById("log")
    const lights = document.getElementById("toggle-lights").checked
    body.setAttribute("style", lights ? `background-color: ${LIGHT_BACKGROUND}; color: ${LIGHT_TEXT}` : `background-color: ${DARK_BACKGROUND}; color: ${DARK_TEXT}`)
    log.setAttribute("style", lights ? `border-color: ${LIGHT_TEXT}` : `border-color: ${DARK_TEXT}`)

    for (let key in colors) {
      colors[key] = get_username_color()
    }

    let usernames = document.getElementsByClassName("chat-username");

    for (let i = 0; i < usernames.length; i++) {
      const username = usernames.item(i);
      const name = username.innerText;
      username.setAttribute("style", `color: ${colors[name]}`)
    }
  }

  document.querySelector("#toggle-timestamps").onclick = function () {
    const chatlog = document.querySelector('#log')
    if (this.checked) {
      chatlog.classList.remove('hide-timestamps')
    } else {
      chatlog.classList.add('hide-timestamps')
    }
  }

  document.querySelector("#toggle-lights").onclick = function () {
    toggle_lights()
  }

  document.querySelector('#send').onclick = function () {
    const textElement = document.querySelector('#text')
    const text = textElement.value

    conn.send(text)
    textElement.value = ''
    textElement.focus()
    return false
  }

  document.querySelector('#text').onkeyup = function (e) {
    if (e.keyCode === 13) {
      document.querySelector('#send').click()
      return false
    }
    return false
  }

  connect()
  toggle_lights()
})()
