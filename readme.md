# DIOS

The purpose of this project is to demonstrate an alternative to centralized
streaming platforms like Twitch.  Instead of a very large, self-contained
ecosystem, DIOS is aimed at **small, self-governed streaming communities.**  By
design, only one person can stream on a DIOS server at any given time – the
**inability to scale** is a feature that is necessary for any [sustainable
online community](https://runyourown.social/#keep-it-small).

While DIOS is similar in spirit to federated platforms like
[Mastodon](https://joinmastodon.org/) and [PeerTube](https://joinpeertube.org/),
federation is not planned for this project.

DIOS is currently in a very early phase of development, but an example of it in
action can be seen at <https://trash.cloud>.  Contributions are very welcome,
whether code, documentation, or simply feedback!

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [DIOS](#dios)
    - [what dios is not](#what-dios-is-not)
    - [developing](#developing)
    - [streaming](#streaming)
    - [architecture](#architecture)
        - [postgres](#postgres)
        - [nginx-rtmp](#nginx-rtmp)
    - [deployment](#deployment)

<!-- markdown-toc end -->


## what dios is not ##

* **a twitch clone**.  We're not trying to replicate commercial platforms like
    Twitch or Mixer, particularly those features intended to increase revenue at
    the expense of community or individual privacy. We also don't have the
    resources or interest in ticking every feature box.
* **federated**.  We don't have anything against a federated streaming service.
    Our focus is simply on supporting small communities, rather than connecting
    them.
* **licensed for capitalist use**.  Not only are our goals opposed to
    for-profit streaming platforms, capitalist use is actively forbidden by our
    license, the [Anti-Capitalist Software
    License](https://anticapitalist.software/).  While we definitely don't want
    exploitative for-profits using DIOS, if your business is worker-owned and
    democractically operated but concerned you technically count as "for
    profit," drop us a line.


## developing

**without docker:**
1. `cp .env.template to .env.`
1. set up a local PostgreSQL instance and modify the `POSTGRES_*` variables in
`.env` appropriately.
1. optionally, set up a local instance of [nginx-rtmp](https://github.com/arut/nginx-rtmp-module)
1. `cargo install systemfd cargo-watch` (for autoreloading; see <https://actix.rs/docs/autoreload/>)
1. `scripts/server.sh`

**with docker**
```
docker-compose up
```

To designate accounts as streamers with superadmin privileges, use
`scripts/admin.sh` and `scripts/unadmin.sh`.

## streaming

Configure OBS etc. to stream to `rtmp://host.name/live`, with the stream key set
by `STREAM_KEY`.

## architecture

![DIOS architecture](./docs/architecture.png)

### postgres

The application talks to the PostgreSQL database via
[Diesel](https://diesel.rs/).  Existing migrations will be automatically run on
startup, but if you add new ones you will have to perform a manual migration:

```shell
cargo install diesel_cli --no-default-features --features postgres
diesel migration generate new_migration
diesel migration run
```

### nginx-rtmp

We use NGINX with the [nginx-rtmp](https://github.com/arut/nginx-rtmp-module)
module as a dedicated RTMP server.  HTTP traffic is not handled by this NGINX
server; if deployed with Helm, the Rust webserver sits behind a [K8s
ingress](https://kubernetes.github.io/ingress-nginx/).

## deployment

To deploy using [Helm](https://helm.sh), either edit `charts/values.yaml` or
create a custom values file, e.g.:

```yaml
dios:
  app_domain: your.cool.tld
  app_title: 'not twitch'

rtmp:
  stream_key: super-secret-stream-key

smtp:
  enabled: true
  from_address: hello.from@your.cool.tld
  hostname: smtp.nice
  password: dont-tell-me
  username: big-mailer

# this may be different, depending on how your ingress is configured
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
  hosts:
    - host: your.cool.tld
      paths: ['/']
  tls:
    - secretName: cool-secret
      hosts:
        - your.cool.tld
```

You can then deploy with Helm:
```shell
helm install dios ./chart -n dios --create-namespace -f your/values/file.yaml
```

Note that **if you are using the K8s ingress controller,** allowing traffic to
port 1935 for RTMP will require [manual
configuration](https://garbage.world/posts/ports/).
