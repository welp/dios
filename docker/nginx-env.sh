#!/bin/sh

envsubst '${APP_HOST} ${APP_PORT}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf

exec "$@"
